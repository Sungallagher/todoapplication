﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AppService;
using T = AppData.Model;
using System.Threading.Tasks;

namespace RestAPI.Controllers
{
    public class TaskController : ApiController
    {
        private ITaskService _taskService = new TaskService();

       
        [Route("api/task")]
        [HttpGet]
        [ResponseType(typeof(T.Task))]
        public async Task<IHttpActionResult> GetAllTasks()
        {
           return  Ok(await _taskService.GetAllTasks());
        }

        // GET api/Task/{Id}
        [HttpGet]
        [Route("api/task/{id}")]
        [ResponseType(typeof(T.Task))]
        public async Task<IHttpActionResult> GetTaskById(string Id)
        {
            var task = await _taskService.GetTaskById(Convert.ToInt32(Id));
            if (task == null)
            {
                return NotFound();
            }
            return Ok(task);
        }

        [HttpPut]
        [Route("api/task/edit")]
        [ResponseType(typeof(T.Task))]
        public async Task<IHttpActionResult> EditTask(T.Task task)
        {
            var updateTask = new T.Task();
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            else
            {
                try
                {
                    updateTask = await _taskService.UpdateTask(task.ID, task);
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    throw;
                }
                
            }
            return Ok(updateTask);
        }

        [HttpPut]
        [Route("api/task/settaskstatus/{id}/{status}")]
        public async Task<IHttpActionResult> SetTaskStatus(int Id, int status)
        {
           
            try
            {
                await _taskService.UpdateStatus(Id, status);

            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw;
            }

            return Ok();
        }

        [Route("api/task")]
        [HttpPost]
        [ResponseType(typeof(T.Task))]
        public async Task<IHttpActionResult> CreateTask(T.Task task)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            else
            {
                try
                {
                    await _taskService.CreateTask(task);
                }
                catch (DbUpdateException ex)
                {
                    throw;
                }
                
            }

            return Ok(task);
        }


        [Route("api/task/{id}")]
        [HttpDelete]
        public IHttpActionResult DeleteTask(int Id)
        {
          
            try
            {
                _taskService.DeleteTask(Id);

            }
            catch (Exception ex)
            {
                if (!_taskService.IsExistingTask(Id))
                {
                    return NotFound();
                }
                else {
                    throw;
                }
                
            }
            return Ok();
        }
    }
}
