﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using T = AppData.Model;
using System.Data;
using System.Data.Entity;
using System.Threading.Tasks;

namespace AppService
{
    public class TaskService : ITaskService
    {
        private T.TaskEntities TaskDbContext = new T.TaskEntities();

        public async Task<List<T.Task>> GetAllTasks()
        {
            return await TaskDbContext.Tasks.ToListAsync();
        }

        public async Task<T.Task> GetTaskById(int Id)
        {
            var task = await TaskDbContext.Tasks.FindAsync(Id);
            if (task != null)
            {
                return task;
            }
            return null;
        }

        public async Task<bool> UpdateStatus(int Id,int NewStatus)
        {
            var task = await TaskDbContext.Tasks.FindAsync(Id);
            if (task != null)
            {
                task.TaskStatus = NewStatus;
                
                await TaskDbContext.SaveChangesAsync();

                return true;
            }

            return false;
        }

        public async Task<T.Task> UpdateTask(int Id, T.Task NewTask)
        {
            var originalTask = await TaskDbContext.Tasks.FindAsync(Id);
            if (originalTask != null)
            {
                originalTask.Name = NewTask.Name;
                originalTask.TaskDate = NewTask.TaskDate;
                originalTask.TaskDescription = NewTask.TaskDescription;
                originalTask.TaskStatus = NewTask.TaskStatus;
                originalTask.ModifiedDate = DateTime.Now;
                await TaskDbContext.SaveChangesAsync();
                return originalTask;
            }

            return null;
        }

        public async Task<T.Task> CreateTask(T.Task NewTask)
        {
            TaskDbContext.Tasks.Add(NewTask);

            await TaskDbContext.SaveChangesAsync();

            return NewTask;
        }

        public void DeleteTask(int Id)
        {
            var task = TaskDbContext.Tasks.Find(Id);
            if (task != null)
            {
                TaskDbContext.Tasks.Remove(task);
                TaskDbContext.SaveChanges();
            }
        }

        public bool IsExistingTask(int Id)
        {
            T.Task task = TaskDbContext.Tasks.Find(Id);
            if (task != null)
            {
                return true;
            }

            return false;
        }
    }
}