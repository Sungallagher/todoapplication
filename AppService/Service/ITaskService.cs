﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using T = AppData.Model;

namespace AppService
{
    public interface ITaskService
    {
        Task<List<T.Task>> GetAllTasks();

        Task<T.Task> GetTaskById(int Id);

        Task<bool> UpdateStatus(int Id, int NewStatus);

        Task<T.Task> UpdateTask(int Id, T.Task NewTask);

        Task<T.Task> CreateTask(T.Task NewTask);

        void DeleteTask(int Id);

        bool IsExistingTask(int Id);
    }
}