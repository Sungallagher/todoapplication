# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Using Tools
* How to set up the project
* API Description



### How do I get set up? ###

* Install Visual Studio 2015
* Create Web API project(TodoApp, Appdata, AppService)
* Install EF library in project file via nuget
* Add Appdata and AppService lib to TODOAPP reference also add Appdata  lib to AppService project reference as well
* Install SQL Express and SQL Management Studio
* Run Sqcript TaskTable.sql to create table
* Download Postman


### API Description ###

* **GET**
	* **api/task** - view all existing tasks
	* **api/task/{id}** - view single task in a list
* **POST**
	* **api/task** - add a task to the list
* **PUT**
	* **api/task/settaskstatus/{id}/{status}** - set task status
	* **api/task/edit** - edit existing task
* **DELETE**
	* **api/task/{id}** - delete a task from the list
