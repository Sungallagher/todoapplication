create table Tasks
(
	ID INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	Name nvarchar(50) not null,
	TaskDescription nvarchar(max) null,
	TaskStatus int not null,
	TaskDate datetime null,
	CreatedDate datetime not null,
	ModifiedDate datetime null
)

ALTER TABLE Tasks ADD CONSTRAINT
DF_Tasks_CreatedDateTime DEFAULT GETDATE() FOR CreatedDate
GO

-- 0 depending, 1 success
insert into Tasks (Name,TaskDescription,TaskStatus, TaskDate)
values ('Buy food', 'Buy Noodles', 0, '2017-06-01')